package com.afkl.exercises.spring.assignment;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Slf4j
@RestController
public class TokenGenerator {

    /** Method to generate oauth token programatically so that user view can make use of it
     *  by default loading through UI for calling the underlying APIs.
     *
     * @return
     */
    @RequestMapping(value = "/generate", method = GET)
    public String generateAuthToken() {

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("username", "travel-api-client");
        headers.add("grant_type", "client_credentials");
        headers.add("password", "psw");
        headers.add("authorities", "ROLE_CLIENT,ROLE_TRUSTED_CLIENT");

        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

        ResponseEntity<String> response
                = restTemplate
                .exchange("http://localhost:8080/oauth/token", HttpMethod.POST, entity, String.class);

        String[] tempList = response.getBody().split("[\\{\\,\\}:\"]");
        String token = null;
        for (String s : tempList) {
            if(s.contains("-")) {
                token = s;
            }
        }

        return token; 
    }
}
