package com.afkl.exercises.spring.assignment;

import com.afkl.exercises.spring.locations.AirportController;
import com.afkl.exercises.spring.locations.Location;
import com.afkl.exercises.spring.paging.Pageable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Stream;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Slf4j
@RestController
public class UserController {

    @Autowired
    private AirportController airportController;

    /*@RequestMapping(value = "/locationomap", produces = MediaType.APPLICATION_JSON_VALUE, method = GET, params = "location")
    public HashMap<String, Location> loadAllAirportsLocationsObjects(@RequestParam("location") String queryString)
            throws Exception {
        Stream<Location> locationStream = findMatchingAirports(queryString);

        HashMap<String, Location> locationMap = new HashMap<>();
        locationStream.forEach(location -> locationMap.put(location.getCode(), location));

        log.info("Location codes are: " + locationMap.toString());

        return locationMap;
    }*/

    /**
     * URI to be called from UI which will load appropriate results according to keystrokes/value entered by user
     * @param queryString
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/locationpicker", produces = MediaType.APPLICATION_JSON_VALUE, method = GET, params = "location")
    public List<String> loadAllAirportsLocationsList(@RequestParam("location") String queryString)
            throws Exception {
        Stream<Location> locationStream = findMatchingAirports(queryString);

        List<String> locationCodes = new ArrayList<>();
        locationStream.forEach(location -> locationCodes.add(location.getCode() + " " + location.getName() + " " + location.getDescription()));

        log.debug("Location codes are: " + locationCodes.toString());

        return locationCodes;
    }

    /**
     * Method to to pick up appropriate list of location objects as a Stream
     * @param queryString
     * @return
     * @throws Exception
     */
    private Stream<Location> findMatchingAirports(String queryString) throws Exception {
        Pageable<Location> pageable = new Pageable<>(1, 1000);
        Callable<HttpEntity<PagedResources<Resource<Location>>>> allAirports = airportController.find("en", queryString, pageable);

        return allAirports.call().getBody().getContent().stream().map(Resource::getContent);
    }
}
