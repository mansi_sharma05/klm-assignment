package com.afkl.exercises.spring.assignment;

import lombok.extern.slf4j.Slf4j;
import org.omg.CORBA.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Slf4j
@RestController
public class MetricsController {
 
    @Autowired
    private TokenGenerator tokenGenerator;

    /**
     * URI to pick up the details from actuator exposed URI: /metrics
     * It could be made better using latest versions of Spring Boot which provide metrics in a detailed list.
     * And then we can use another boor project to create a nice UI to show all those metrics.
     *
     * @param token
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/show/requestlog", method = GET)
    public String loadAllMetrics(@RequestHeader(value = "Authorization") String token) throws Exception {

        //String generatedToken = tokenGenerator.generateAuthToken();
        /*log.debug("Token generated is: " + generatedToken);
        headers.add("Authorization", generatedToken);*/

        log.debug("Token being passed down is: " + token);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("Authorization", token);

        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response
                = restTemplate.exchange("http://localhost:8080/metrics", HttpMethod.GET, entity, String.class);

        // Using String array for better control but at the expense of increased cyclomatic complexity.
        // This could be improved using Hashmap and JSON based processing
        String[] stringArray = response.getBody().split(",");
        int totalRequests = 0;
        int request200 = 0;
        int request400 = 0;
        int request500 = 0;
        double totalResponseTime = 0;
        double minResponse = 0;
        double maxResponse = 0;
        for (String s : stringArray) {
            String[] tempArray = null;

            if (s.contains("counter")) {
                tempArray = s.split("[\\:\\{\\}]");
                int index = 0;
                if (tempArray.length == 3 && tempArray[0] == "") {
                    index = 1;
                }
                if (tempArray[index].contains("2")) {
                    request200 = request200 + Integer.parseInt(tempArray[index + 1]);
                } else if (tempArray[index].contains("4")) {
                    request400 = request400 + Integer.parseInt(tempArray[index + 1]);
                } else {
                    request500 = request500 + Integer.parseInt(tempArray[index + 1]);
                }
            } else if (s.contains("response")) {
                tempArray = s.split("[\\:\\{\\}]");
                int index = 0;
                if (tempArray.length == 3 && tempArray[0] == "") {
                    index = 1;
                }
                double responseTime = Double.parseDouble(tempArray[index + 1]);
                totalResponseTime = totalResponseTime + responseTime;
                if (minResponse == 0 || minResponse > responseTime) {
                    minResponse = responseTime;
                }
                if (maxResponse == 0 || maxResponse < responseTime) {
                    maxResponse = responseTime;
                }
            }
        }

        totalRequests = request200 + request400 + request500;
        double averageResponseTime = totalResponseTime / totalRequests;

        StringBuilder finalResponse = new StringBuilder("{");
        finalResponse = finalResponse.append("\"totalRequestsProcessed\":").append(String.valueOf(totalRequests));
        finalResponse = finalResponse.append(",\"repliedWith200\":").append(String.valueOf(request200));
        finalResponse = finalResponse.append(",\"repliedWith400\":").append(String.valueOf(request400));
        finalResponse = finalResponse.append(",\"repliedWith500\":").append(String.valueOf(request500));
        finalResponse = finalResponse.append(",\"averageResponseTime\":").append(String.valueOf(averageResponseTime));
        finalResponse = finalResponse.append(",\"minimumResponseTime\":").append(String.valueOf(minResponse));
        finalResponse = finalResponse.append(",\"maximumResponseTime\":").append(String.valueOf(maxResponse));
        finalResponse = finalResponse.append("}");

        return finalResponse.toString(); 
    }
}
