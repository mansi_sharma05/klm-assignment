package com.afkl.exercises.spring.fares;

import com.afkl.exercises.spring.locations.AirportRepository;
import com.afkl.exercises.spring.locations.Location;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static java.math.RoundingMode.HALF_UP;
import static java.util.Locale.ENGLISH;
import static java.util.Locale.getAvailableLocales;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/fares/{origin}/{destination}")
public class FareController {

    private final AirportRepository repository;

    @Autowired
    public FareController(AirportRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(method = GET)
    public Callable<Fare> calculateFare(@PathVariable("origin") String origin,
            @PathVariable("destination") String destination,
            @RequestParam(value = "currency", defaultValue = "EUR") String currency) {
        return () -> {
            Thread.sleep(ThreadLocalRandom.current().nextLong(1000, 6000));

            //final Location o = repository.get(ENGLISH, origin).orElseThrow(IllegalArgumentException::new);
            //final Location d = repository.get(ENGLISH, destination).orElseThrow(IllegalArgumentException::new);

            List<String> locations = new ArrayList<>();
            locations.add(origin);
            locations.add(destination);

            final Map<String, Location> locationMap = getLocationsParallel(locations);

            final BigDecimal fare = new BigDecimal(ThreadLocalRandom.current().nextDouble(100, 3500))
                    .setScale(2, HALF_UP);

            return new Fare(fare.doubleValue(), Currency.valueOf(currency.toUpperCase()), locationMap.get(origin),
                    locationMap.get(destination));
        };
    }

    public Map<String, Location> getLocationsParallel(List<String> locations) {

        List<CompletableFuture<Location>> futures = locations.stream().map(location -> retriveLocationAsync(location))
                .collect(Collectors.toList());

        Map<String, Location> result = futures.stream().map(CompletableFuture::join).collect(Collectors.toMap(l -> l.getCode(), l -> new Location(l.getCode(),l.getName(),l.getDescription(), null, null, null)));

        return result;
    }

    private CompletableFuture<Location> retriveLocationAsync(String locationCode) {

        CompletableFuture<Location> future =
                CompletableFuture.supplyAsync(new Supplier<Location>() {
                     @Override
                    public Location get() {
                        final Location location = repository.get(ENGLISH, locationCode).orElseThrow(IllegalArgumentException::new);
                        return location;
                    }
                });

        return future;
    }

}
