package com.afkl.exercises.spring.fares;

import com.afkl.exercises.spring.locations.Location;
import lombok.Value;

import java.util.Map;

@Value
public class Fare {

    double amount;
    Currency currency;
     Location origin, destination;

}
